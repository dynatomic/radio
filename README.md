# Frequency and Band

---

## Chirp Support

https://chirp.danplanet.com/projects/chirp/wiki/Supported_Radios

## Frequency

| Band   | Frequency Range | National Simplex
| ---    | ---             | ---
| 2  M   | 144 - 148 MHz   | 146.520
| 70 cm  | 420 - 450 MHz   | 446.000

---

## CB Frequencies (26.965 to 27.405 MHz)

| Channel | Frequency | Descripton |
| ---     | ---       | ---        |
| 9       | 27.065    | Emergency communications
| 17      | 27.165    | North - South
| 19      | 27.185    | East  - West

---

## Marine Frequencies (156 - 174 MHz)

| Channel | Frequency | Descripton |
| ---     | ---       | ---        |
| 16      | 156.800   | Emergency and Distress |
| 68      | 156.425   | Boat 2 Boat
| 69      | 156.475   | Boat 2 Boat
| 71      | 156.575   | Boat 2 Boat
| 72      | 156.625   | Boat 2 Boat

---

## GMRS Frequencies (462 MHz and 467 MHz)

| Frequency     | Channel | FRS Power |  FRS Bandwidth | GMRS power | GMRS Bandwidth
| ---           | ---     | ---       | ---            | ---        | ---
| 462.5625 MHz  | 1       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 462.5875 MHz  | 2       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 462.6125 MHz  | 3       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 462.6375 MHz  | 4       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 462.6625 MHz  | 5       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 462.6875 MHz  | 6       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 462.7125 MHz  | 7       | 2 W       | 12.5 kHz       | 5 W        | 20 kHz
| 467.5625 MHz  | 8       | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 467.5875 MHz  | 9       | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 467.6125 MHz  | 10      | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 467.6375 MHz  | 11      | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 467.6625 MHz  | 12      | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 467.6875 MHz  | 13      | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 467.7125 Mhz  | 14      | 0.5 W     | 12.5 kHz       | 0.5 W      | 12.5 kHz
| 462.5500 MHz  | 15      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.5750 MHz  | 16      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.6000 MHz  | 17      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.6250 MHz  | 18      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.6500 MHz  | 19      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.6750 MHz  | 20      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.7000 MHz  | 21      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 462.7250 MHz  | 22      | 2 W       | 12.5 kHz       | 50 W       | 20 kHz
| 467.5500 MHz  | 15R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.5750 MHz  | 16R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.6000 MHz  | 17R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.6250 MHz  | 18R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.6500 MHz  | 19R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.6750 MHz  | 20R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.7000 MHz  | 21R     | N/A       | N/A            | 50 W       | 20 kHz
| 467.7250 MHz  | 22R     | N/A       | N/A            | 50 W       | 20 kHz

---

## NOA

| Name | Frequency
| ---  | ---
| WX01 | 162.550 MHz
| WX02 | 162.400 MHz
| WX03 | 162.475 MHz
| WX04 | 162.425 MHz
| WX05 | 162.450 MHz
| WX06 | 162.500 MHz
| WX07 | 162.525 MHz
| WX08 | 161.650 MHz
| WX09 | 161.775 MHz
| WX10 | 163.275 MHz

## Carla `https://www.carlaradio.net`

| Systems   | Location          | Freq      | Link  | Local |
| ---       | ---               | ---       | ---   | ---   |
| 02        | San Francisco     | 442.075+  | 162.2 | 100.0 |
| 03        | Pt. Arena         | 443.075+  | 173.8 | 100.0 |
| 04        | Greenfield        | 442.075+  | 167.9 | 114.8 |
| 05        | Oakland           | 443.050+  | 173.8 | 114.8 |
| 06        | Willows           | 443.075+  | 167.9 | 114.8 |
| 07        | San Jose          | 443.075+  | 162.2 | 123.0 |
| 08        | South Lake Tahoe  | 442.075+  | 151.4 | 127.3 |
| 09        | Angels Camp       | 442.075+  | 173.8 | 123.0 |
| 10        | Monterey Bay      | 443.475+  | 173.8 | 123.0 |
| 11        | Walnut Creek      | 443.475+  | 162.2 | 114.8 |
| 12        | Pleasanton        | 442.075+  | 156.7 | 103.5 |
| 13        | Shasta Lake       | 442.075+  | 167.9 | 114.8 |
| 14        | Cisco Grove       | 443.475+  | 156.7 | 100.0 |
| 15        | Fresno            | 440.750+  | 162.2 | 114.8 |
| 16        | Sacramento        | 440.750+  | 173.8 | 107.2 |
| 17        | Sonora            | 443.475+  | 151.4 | 103.5 |
| 18        | Topaz Lake        | 443.475+  | 167.9 | 110.9 |
| 19        | Hayward           | 443.325+  | 173.8 | 114.8 |
| 20        | Burney            | 440.750+  | 156.7 | 123.0 |
| 21        | Reno              | 440.750+  | 156.7 | 127.3 |
| 22        | Hawthorne         | 440.725+  | 156.7 | 127.3 |
| 23        | North Lake Tahoe  | 441.550+  | 156.7 | 127.3 |
| 24        | Carson Valley     | 443.325+  | 156.7 | 127.3 |
| 25        | Redding           | 444.325+  | 173.8 | 100.0 |
| 26        | Geyserville       | 443.475+  | 167.9 | 110.9 |
| 27        | Quincy            | 440.725+  | 173.8 | 100.0 |
| 28        | Mt. Shasta        | 443.475+  | 173.8 | 107.2 |
| 29        | San Luis Obispo   | 443.500+  | 162.2 | 100.0 |
| 30        | Bakersfield       | 444.900+  | 162.2 | 127.3 |
| 31        | Los Angeles       | 448.080-  | 162.2 | 88.5  |
| 32        | Pacifica          | 440.725+  | 167.9 | 114.8 |
| 33        | Gualala           | 442.075+  | 141.3 | 131.8 |
| 35        | Oakland           | 146.850-  | 162.2 | 103.5 |
| 36        | Willows           | 146.115+  | 173.8 | 123.0 |

## Local Bay Area Repeaters

| Frequency    | Offset | Tone In/Out             | Location                        | Call   | Modes
| ---          | ---    | ---                     | ---                             | ---    | ---
| 442.700000   | +5 MHz | 173.8 / 173.8           | South San Francisco - Howard    | N6MNV  | FM
| 440.050000   | +5 MHz | NAC 293 100.0 / 100.0   | San Mateo, College of San Mateo | K7CBL  | FM P-25
| 440.550000   | +5 MHz | 114.8 / 114.8           | San Bruno, city hall            | K6PVJ  | FM
| 441.250000   | +5 MHz | 141.3 / 141.3           | South San Francisco             | K6DNA  | FM

---
# Cable

```
The attenuation chart lists the amount of loss in decibels (Db) per 100 feet for a given frequency. Loss values are a multiplier, so a 200 foot feedline run would have double the loss at a given frequency. Likewise a 50 foot run would have half the loss.
```

Cable            | 10 MHz | 30 Mhz | 50 Mhz | 100 Mhz | 150 MHz | 220 MHz | 450 MHz
| ---            | ---    | ---    | ---    | ---     | ---     | ---     | ---
| RG-174         | 3.3    | 5.5    | 6.6    | 8.9     | 13      | 17.3    | 25
| RG-58          | 1.4    | 2.5    | 3.1    | 4.9     | 6.2     | 7.4     | 10.6
| RG-8X          | 1.0    | 2.0    | 2.5    | 3.6     | 4.7     | 6.0     | 8.6
| RG-8U          | 0.6    | 1.2    | 1.6    | 2.2     | 2.8     | 3.5     | 5.2
| Belden 9913    | 0.4    | 0.8    | 1.1    | 1.3     | 1.7     | 2.1     | 3.1
| Belden 9914    | 0.4    | 0.8    | 1.1    | 1.4     | 1.7     | 2.1     | 3.1
| LMR-400        | 0.4    | 0.8    | 0.9    | 1.2     | 1.5     | 1.8     | 2.7
| 1/2″ Superflex | 0.3    | .56    | 0.73   | 1.04    | 1.3     | 1.6     | 2.3
